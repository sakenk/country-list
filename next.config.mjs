/** @type {import('next').NextConfig} */
const nextConfig = {
    // disable next/font plugin
    // experimental: {
    //     optimizeFonts: false,
    // },
};

export default nextConfig;
