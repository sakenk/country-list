import { useState } from 'react';
import { Country } from '../models/country.model';

// export interface Country {
//     id?: number;
//     code: string;
//     name: string;
//     currencies: string[];
//     active?: boolean;
// }

interface Props {
    country: Country;
    active: boolean;
    toggleActive: (countryCode: string) => void;
}

const CountryListItem: React.FC<Props> = ({ country, active, toggleActive }) => {
    const [isActive, setIsActive] = useState(active);

    const handleToggleActive = () => {
        setIsActive(!isActive);
        toggleActive(country.code);
    };

    return (
        <div style={{margin: '5px 0'}}>
            <div>{country.name}</div>
            <div>Валюты: {country.currencies}</div>
            <div>
                {/* Active: */}
                Активно:
                <input
                    type="checkbox"
                    checked={isActive}
                    onChange={handleToggleActive}
                />
            </div>
        </div>
    );
};

export default CountryListItem;
