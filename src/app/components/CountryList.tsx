"use client";

import { useState, useEffect } from 'react';
import CountryListItem from './CountryListItem';
import { Country } from '../models/country.model';

interface Props {
    countries: Country[];
}

const CountryList: React.FC<Props> = ({ countries }) => {
    const [searchTerm, setSearchTerm] = useState<string>('');

    // Filter countries based on the search term
    const filteredCountries = countries.filter(country =>
        country.name.toLowerCase().includes(searchTerm.toLowerCase())
    );

    // Handle changes in the search input
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(event.target.value);
    };

    // State to store active/inactive status
    const [activeCountries, setActiveCountries] = useState<{ [key: string]: boolean }>(() => {
        // eslint-disable-next-line no-undef
        const activeCountriesFromStorage = localStorage.getItem('activeCountries');
        return activeCountriesFromStorage ? JSON.parse(activeCountriesFromStorage) : {};
    });

    // Update localStorage whenever activeCountries changes
    useEffect(() => {
        // eslint-disable-next-line no-undef
        localStorage.setItem('activeCountries', JSON.stringify(activeCountries));
    }, [activeCountries]);

    // Function to handle toggle of active/inactive
    const toggleActive = (countryCode: string) => {
        setActiveCountries(prevActiveCountries => ({
            ...prevActiveCountries,
            [countryCode]: !prevActiveCountries[countryCode]
        }));
    };

    return (
        <center>
            <h1>Список стран</h1>
            <input
                type="text"
                placeholder="Введите название страны"
                value={searchTerm}
                onChange={handleChange}
            />
            {filteredCountries.map((country) => (
                <CountryListItem
                    // key={country.code}
                    key={country.id}
                    country={country}
                    active={activeCountries[country.code]}
                    toggleActive={toggleActive}
                />
            ))}
        </center>
    );
};

export default CountryList;
