// CountryList.test.tsx
import React from 'react';
import { render, screen } from '@testing-library/react';
import CountryList from '../CountryList';
import { Country } from '@/app/models/country.model';

describe('CountryList component', () => {
  test('renders list of countries', () => {
    const countries: Country[] = [
      { code: 'US', name: 'United States', currencies: ['USD'] },
      { code: 'CA', name: 'Canada', currencies: ['CAD'] },
      // Add more countries as needed
    ];

    render(<CountryList countries={countries} />);
    // Add your assertions here
  });
});


// describe('CountryList component', () => {
//   test('renders a list of countries', async () => {
//     const mockCountries = [
//       { name: 'Country 1' },
//       { name: 'Country 2' },
//       { name: 'Country 3' }
//     ];

//     // Mocking fetch function to return mockCountries
//     global.fetch = jest.fn().mockResolvedValue({
//       ok: true,
//       json: () => Promise.resolve(mockCountries)
//     });

//     render(<CountryList />);

//     // Wait for the component to fetch and render the countries
//     const countryListItems = await screen.findAllByRole('listitem');

//     // Asserting that the correct number of countries is rendered
//     expect(countryListItems).toHaveLength(mockCountries.length);

//     // Asserting that each country name is rendered
//     mockCountries.forEach(country => {
//       expect(screen.getByText(country.name)).toBeInTheDocument();
//     });
//   });

//   test('handles fetch error', async () => {
//     // Mocking fetch function to simulate a failed request
//     global.fetch = jest.fn().mockResolvedValue({
//       ok: false,
//       statusText: 'Not Found'
//     });

//     render(<CountryList />);

//     // Asserting that error message is rendered
//     const errorMessage = await screen.findByText('Error fetching countries:');
//     expect(errorMessage).toBeInTheDocument();
//   });
// });
