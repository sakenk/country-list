import { promises as fs } from 'fs';

import CountryList from './components/CountryList';

export default async function Page() {
    const data = await getData();

    return (
        <div>
            <CountryList countries={data.countries} />
        </div>
    );
}

async function getData() {
    const file = await fs.readFile(process.cwd() + '/data/dummy-backend.json', 'utf8');
    return JSON.parse(file);
}